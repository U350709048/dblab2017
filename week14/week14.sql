# intial time 0,280
# after index 0,197
# after Pk 0,043

use exercise;

load data local infile '/home/diablo/Desktop/insert.txt' into table Proteins fields terminated by '|';

select *
from Proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on Proteins (uniprot_id);

drop index uniprot_index on Proteins;

alter table Proteins add constraint pk_proteins primary key (uniprot_id); 