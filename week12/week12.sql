use company;

#For each employee, get the employee last name and the last name of their department manager.

select e1.last_name,e2.last_name
from employee e1 join employee e2 ,department d
where e1.dno=d.dnumber and d.mgrssn=e2.ssn and e1.last_name !=e2.last_name
order by e1.last_name asc;
#Salaries of all employees have been incremented by $1000 plus 3% of their previous salaries.
update employee set salary =salary+ salary*3/100+1000;
#Get the full names of employees who are working on more than one project.
select first_name,last_name
from employee
where ssn in(
	select w1.ssn
    from works_on 
    
    group by essn
    having count(distinct pno>1)
